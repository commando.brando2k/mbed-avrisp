#include "stk500v1.h"

Stk500v1* Stk500v1::handler = {0};

void Stk500v1::irqReceiveCommand() {

    uint8_t byte = this->_serial.getc();
    this->_command_buffer.push(byte);

    if (byte == STK_CRC_EOP) {
        this->_crc_eop_received = true;
    }
    //this->_queue.call(&Stk500v1::crcEopHandler);
}

bool Stk500v1::receive(uint8_t& cmd) {
    bool success = this->_command_buffer.pop(cmd);

    return success;
}

bool Stk500v1::receive(uint8_t* buf, size_t num_bytes) {
    bool success = true;
    for (uint8_t idx = 0; idx < num_bytes; ++idx) {
        if (!this->receive(buf[idx])) {
            buf[idx] = 0;
            success = false;
        }
    }

    return success;
}

void Stk500v1::send_cmd(uint8_t cmd) {
    this->_serial.putc(cmd);
}

void Stk500v1::send_msg(uint8_t msg) {
    uint8_t eop;
    this->receive(eop);
    if (eop != STK_CRC_EOP) {
        this->send_cmd(STK_NOSYNC);
        return;
    } else {
        this->send_cmd(STK_INSYNC);
    }

    this->send_cmd(msg);
    this->send_cmd(STK_OK);
}

void Stk500v1::send_msg(uint8_t* msg, size_t len) {
    uint8_t eop;
    this->receive(eop);
    if (eop != STK_CRC_EOP) {
        this->send_cmd(STK_NOSYNC);
        return;
    } else {
        this->send_cmd(STK_INSYNC);
    }
    
    if (msg != NULL) {
        for (uint8_t idx = 0; idx < len; ++idx) {
            this->send_cmd(msg[idx]);
        }
    }
        
    this->send_cmd(STK_OK);
}

void Stk500v1::set_device_ext() {
    uint8_t len;
    this->receive(len);
    this->_device.ext = new uint8_t[--len]; // DELETEME!!
    this->receive(_device.ext, len);

    this->send_msg();
}

void Stk500v1::set_device() {
    this->receive(_buffer, 20);

    _device.devicecode     = _buffer[0];
    _device.revision       = _buffer[1];
    _device.progtype       = _buffer[2];
    _device.parmode        = _buffer[3];
    _device.polling        = _buffer[4];
    _device.selftimed      = _buffer[5];
    _device.lockbytes      = _buffer[6];
    _device.fusebytes      = _buffer[7];
    _device.flashpollval   = _buffer[8];
    _device.eeprompollval  = _buffer[10];
    _device.pagesize       = (_buffer[12] << 8) | _buffer[13];
    _device.eepromsize     = (_buffer[14] << 8) | _buffer[15];
    _device.flashsize      = (_buffer[16] << 24) | 
                             (_buffer[17] << 16) | 
                             (_buffer[18] << 8) | _buffer[19];

    if (_device.ext == NULL) {
        _device.ext = 0;
    }
    
    this->send_msg();
}

void Stk500v1::get_parameter() {
    uint8_t type, version = 0;
    this->receive(type);
    switch (type) {
        case STK_GET_HW_VER:
            version = STK_GET_HW_VER;
            break;
        case STK_GET_SW_VER_MAJ:
            version = STK_SW_VER_MAJ;
            break;
        case STK_GET_SW_VER_MIN:
            version = STK_SW_VER_MIN;
            break;
        case STK_GET_IFACE_TYPE:
            version = STK_IFACE_TYPE;
            break;
    }
    this->send_msg(version);
}

uint16_t Stk500v1::get_current_page(uint16_t addr) {
    if (_device.pagesize == 0x20) {
        addr &= 0xfff0;
    } else if (_device.pagesize == 0x40) {
        addr &= 0xffe0;
    } else if (_device.pagesize == 0x80) {
        addr &= 0xffc0;
    } else if (_device.pagesize == 0x100) {
        addr &= 0xff80;
    }
    return addr;
}

void Stk500v1::load_address() {
    this->receive(_addr_low);
    this->receive(_addr_high);

    uint16_t cur_addr = (uint16_t)(_addr_high << 8);
    cur_addr |= (uint16_t)(_addr_low);
    _page = this->get_current_page(cur_addr);

    this->send_msg();
}

bool Stk500v1::prog_page() {
    uint8_t bytes_high, bytes_low, memtype;
    this->receive(&bytes_high, 1);
    this->receive(&bytes_low, 1);
    this->receive(&memtype, 1);

    uint16_t len = (bytes_high << 8) | bytes_low;
    this->receive(this->_buffer, len);

    // Write data in page
    bool success = false;
    if (memtype == 'F') {
        // pass page here
        this->_isp->write_flash_page(_page, this->_buffer, len);
    } else if (memtype == 'E') {
        // program eeprom
    } else {
        success = false;
    }
    return success;
}

void Stk500v1::read_page() {
    uint8_t bytes_high, bytes_low, memtype;
    this->receive(&bytes_high, 1);
    this->receive(&bytes_low, 1);
    this->receive(&memtype, 1);


    uint16_t addr = (_addr_high << 8) | _addr_low;
    uint16_t len = (bytes_high << 8) | bytes_low;

    this->send_cmd(STK_INSYNC);
    for (uint8_t idx = 0; idx < len; idx+=2, ++addr) {
        uint16_t word = this->_isp->read_flash_word(addr);
        this->send_cmd((uint8_t)(word & 0xff));
        this->send_cmd((uint8_t)(word>>8 & 0xff));
    }
    this->send_cmd(STK_OK);
}

void Stk500v1::universal() {
    const uint8_t len = 4;
    uint8_t buf[len];
    this->receive(buf, len);

    uint8_t out = this->_isp->command(buf, len);

    this->send_msg(out);
}

void Stk500v1::parse_cmd() {
    if (!_crc_eop_received) return;
    _crc_eop_received = false;

    uint8_t cmd;
    this->receive(cmd);
    
    switch (cmd) {
        case STK_GET_SYNC:
            this->send_msg();
            break; 
        case STK_GET_SIGN_ON:
            this->send_msg((uint8_t*)"AVR ISP", 7);
            break;
        case STK_SET_PARMAMETER:          // 0x40
            this->send_msg();
            break;
        case STK_GET_PARMAMETER:          // 0x41
            this->get_parameter();
            break;
        case STK_SET_DEVICE:          // 0x42
            this->set_device();
            break;
        case STK_SET_DEVICE_EXT:          // 0x45
            this->set_device_ext();
            break;
        case STK_ENTER_PROGMODE:            // 0x50
            while(!this->_isp->programming_enable());
            this->send_msg();
            break;
        case STK_LEAVE_PROGMODE:            // 0x51
            this->_isp->programming_disable();
            this->send_msg();
            break;
        case STK_LOAD_ADDRESS:              // 0x55
            this->load_address();
            break;
        case STK_UNIVERSAL:                 // 0x56
            this->universal();
            break;
        case STK_PROG_PAGE:                 // 0x64
            this->prog_page();
            this->send_msg();
            break;
        case STK_READ_SIGN:
            this->send_msg(this->_isp->read_signature(), 3);
            break;
        case STK_READ_PAGE:                 // 0x74
            this->read_page();
            break;

        case STK_CRC_EOP:
            this->send_cmd(STK_NOSYNC);
            break;
        default: 
            this->receive(cmd);
            if (cmd == STK_CRC_EOP)
                this->send_cmd(STK_UNKNOWN);
            else
                this->send_cmd(STK_NOSYNC);
    }
    this->_command_buffer.reset();
}

/* vim: set ft=pio: */
