#ifndef __STK500V1_H__
#define __STK500V1_H__

#include "mbed.h"
//#include "rtos.h"
#include "mbed_events.h"
#include "Serial.h"
#include "CircularBuffer.h"
#include "AvrISP/AvrISP.h"

// STK response
#define STK_OK          0x10
#define STK_FAILED      0x11
#define STK_UNKNOWN     0x12
#define STK_NODEVICE    0x13
#define STK_INSYNC      0x14
#define STK_NOSYNC      0x15
#define STK_CRC_EOP     0x20

// STK commands
#define STK_GET_SYNC        0x30
#define STK_GET_SIGN_ON     0x31
#define STK_SET_PARMAMETER  0x40
#define STK_GET_PARMAMETER  0x41
#define STK_SET_DEVICE      0x42
#define STK_SET_DEVICE_EXT  0x45

#define STK_ENTER_PROGMODE  0x50
#define STK_LEAVE_PROGMODE  0x51
#define STK_CHIP_ERASE      0x52
#define STK_CHECK_AUTOINC   0x53
#define STK_LOAD_ADDRESS    0x55
#define STK_UNIVERSAL       0x56
#define STK_UNIVERSAL_MULTI 0x57

#define STK_PROG_PAGE       0x64

#define STK_READ_PAGE       0x74
#define STK_READ_SIGN       0x75

#define STK_GET_HW_VER      0x80
#define STK_GET_SW_VER_MAJ  0x81
#define STK_GET_SW_VER_MIN  0x82
#define STK_GET_IFACE_TYPE  0x93

#define STK_HW_VER          0x2
#define STK_SW_VER_MAJ      0x1
#define STK_SW_VER_MIN      0x2a
#define STK_IFACE_TYPE      0x53

#define STK_MAX_BUFF_SIZE   0x100
#define STK_SERIAL_TX       USBTX
#define STK_SERIAL_RX       USBRX
#define STK_SERIAL_BAUDRATE 115200

// STK device parameters
typedef struct {
    uint8_t devicecode;
    uint8_t revision;
    uint8_t progtype;
    uint8_t parmode;
    uint8_t polling;
    uint8_t selftimed;
    uint8_t lockbytes;
    uint8_t fusebytes;
    uint8_t flashpollval;
    uint8_t eeprompollval;
    uint16_t pagesize;
    uint16_t eepromsize;
    uint32_t flashsize;
    uint8_t* ext;
} parameter_t;

class Stk500v1 {
public:
    Stk500v1(AvrISP* isp, PinName tx, PinName rx, uint32_t baud) :
        _serial(tx, rx, baud) {
        this->_isp = isp;
        this->handler = this;
        this->_serial.attach(&Stk500v1::irqHandler);
        this->_crc_eop_received = false;

        this->_thread.set_priority(osPriorityHigh);
        this->_thread.start(callback(&_queue, &EventQueue::dispatch_forever));
    };

    // Serial interrupt callback
    void irqReceiveCommand();
    static void irqHandler() { handler->irqReceiveCommand(); };
    static void crcEopHandler() { handler->parse_cmd(); };
    static Stk500v1* handler;

    void parse_cmd();

private:
    AvrISP* _isp;
    Serial  _serial;

    parameter_t _device;
    uint8_t     _addr_low = 0;
    uint8_t     _addr_high = 0;
    uint16_t    _page = 0;

    uint8_t _buffer[STK_MAX_BUFF_SIZE];
    CircularBuffer<uint8_t, STK_MAX_BUFF_SIZE> _command_buffer;
    bool _crc_eop_received;
    
    EventQueue  _queue;
    Thread      _thread;

    void get_parameter();

    bool receive(uint8_t& cmd);

    // Fills buf with num_bytes from the transaction buffer
    bool receive(uint8_t* buf, size_t num_bytes);

    // Send a lone STK command to the serial port
    void send_cmd(uint8_t cmd); 

    void send_msg(uint8_t msg);

    void send_msg(uint8_t* msg = NULL, size_t len = 0);

    void set_device_ext();

    void set_device();


    // Returns the current page of addr
    uint16_t get_current_page(uint16_t addr);

    void load_address();
    bool prog_page();
    void read_page(); 

    // Sends a generic ISP instruction
    void universal(); 
};

#endif //__STK500V1_H__
/* vim: set ft=pio: */
