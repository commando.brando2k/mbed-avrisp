#include "AvrISP.h"

AvrISP::AvrISP(PinName mosi, PinName miso, PinName sck, PinName nrst) : 
    _spi(mosi, miso, sck), _nrst(nrst) {
    this->_spi.format(8, 0);
    this->_nrst = 1;

    this->_flashpoll = POLL_RDYBSY;
}

void AvrISP::set_spi_frequency(uint32_t freq) {
    this->_spi.frequency(freq);
}

bool AvrISP::poll() {
    uint8_t msg[] = { this->_flashpoll, 0x00, 0x00, 0x00 };
    uint8_t busy = 1;
    while (busy == 1) {
        // TODO: timeout
        busy = this->command(msg);
    }
    return true;
}

void AvrISP::load_flash_page(uint8_t offset, uint16_t word) {
    uint8_t msg[] = { 
        LOAD_PGM_MEM_PAGE_LOW, 
        0x00, offset, 
        (uint8_t)(word & 0xff) 
    };
    this->command(msg);

    msg[0] = LOAD_PGM_MEM_PAGE_HIGH;
    msg[3] = (uint8_t)((word >> 8) & 0xff);
    this->command(msg);
}

bool AvrISP::write_flash_page(uint16_t page, uint8_t* data, size_t len) {
    uint8_t offset = 0;
    for (uint8_t idx = 0; idx < len; idx+=2, ++offset) {

        uint16_t word = (data[idx+1] << 8) | data[idx];
        this->load_flash_page(offset, word);
    }

    // commit
    uint8_t msg[4] = { 
        WRITE_PGM_MEM_PAGE, 
        (uint8_t)(page>>8 & 0xff), 
        (uint8_t)(page & 0xff), 0x00 
    };
    this->command(msg);

    // poll rdy/bsy
    bool success = this->poll();
    return success;
}

uint16_t AvrISP::read_flash_word(uint16_t addr) {
    uint16_t word;

    uint8_t msg[] = { 
        READ_PGM_MEM_LOW, 
        (uint8_t)(addr >> 8 & 0xff), 
        (uint8_t)(addr & 0xff), 
        0x00
    };
    word = (uint16_t)this->command(msg);

    msg[0] = READ_PGM_MEM_HIGH;
    word |= (uint16_t)(this->command(msg) << 8);

    return word;
}

bool AvrISP::programming_enable() {
    // hold reset
    this->_nrst = 0;
    this->_spi.write(0x00);     // SCK low?
    Thread::wait(20);
    this->_nrst = 1;
    Thread::wait(0.1f);
    this->_nrst = 0;
    Thread::wait(50);

    this->_spi.write(PROGRAMMING_ENABLE_BYTE1);
    this->_spi.write(PROGRAMMING_ENABLE_BYTE2);
    uint8_t enabled = this->_spi.write(0x00);
    this->_spi.write(0x00);

    return (enabled == PROGRAMMING_ENABLE_BYTE2);
}

void AvrISP::programming_disable() {
    this->_nrst = 1;
}

uint8_t* AvrISP::read_signature() {
    for (uint8_t i = 0; i < 3; ++i) {
        this->_spi.write(READ_SIGNATURE);
        this->_spi.write(0x00);
        this->_spi.write(i);
        this->_signature[i] = this->_spi.write(0x00);
    } 

    return this->_signature;
}

uint8_t AvrISP::command(uint8_t* instruction, size_t len) {

    uint8_t response = 0xff;
    for (uint8_t idx = 0; idx < len; ++idx) {
        response = this->_spi.write(instruction[idx]);
    }

    return response;
}

/* vim: set ft=pio: */
