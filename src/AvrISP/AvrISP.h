#ifndef __AVRISP_H__
#define __AVRISP_H__

#include "mbed.h"
#include "rtos.h"

// AVR isp instruction set
enum commands {
    PROGRAMMING_ENABLE_BYTE1 = 0xac,
    PROGRAMMING_ENABLE_BYTE2 = 0x53,
    CHIP_ERASE_BYTE1         = 0xac,
    CHIP_ERASE_BYTE2         = 0x80,

    LOAD_PGM_MEM_PAGE_LOW    = 0x40,
    LOAD_PGM_MEM_PAGE_HIGH   = 0x48,
    LOAD_EEPROM_MEM_PAGE     = 0xc1,

    READ_PGM_MEM_LOW         = 0x20,
    READ_PGM_MEM_HIGH        = 0x28,
    READ_SIGNATURE           = 0x30,

    WRITE_PGM_MEM_PAGE       = 0x4c,

    POLL_RDYBSY              = 0xf0,

};

//-------------------------------------
class AvrISP {
    public:
        AvrISP(PinName mosi, PinName miso, PinName sck, PinName nrst);
        uint8_t command(uint8_t* bytes, size_t len = 4);
        bool programming_enable();
        void programming_disable();
        bool poll();

        uint16_t read_flash_word(uint16_t addr);
        uint8_t* read_signature();

        void load_flash_page(uint8_t offset, uint16_t word);
        bool write_flash_page(uint16_t page, uint8_t* data, size_t len);

        void set_spi_frequency(uint32_t freq);
    private:
        SPI _spi;
        DigitalOut _nrst;
        uint8_t _signature[3];
        uint8_t _flashpoll;
};
//-------------------------------------


#endif // __AVRISP_H__
/* vim: set ft=pio: */
