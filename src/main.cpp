#include "mbed.h"
#include "rtos.h"
#include "AvrISP/AvrISP.h"
#include "STK/stk500v1.h"

#include "FastPWM.h"

#define TARGET_CLK 1000000L
#define SPI_CLK (TARGET_CLK / 6)

#define AVRISP_TX   USBTX
#define AVRISP_RX   USBRX
#define AVRISP_BAUD 115200

#ifdef __SPI1
#define MOSI SPI_MOSI
#define MISO SPI_MISO
#define SCK  SPI_SCK
#elif defined __SPI2 
#define MOSI PB_15
#define MISO PB_14
#define SCK  PB_13
#else
#define MOSI PC_12
#define MISO PC_11
#define SCK  PC_10
#endif
#define RST  PA_8

#ifndef __SPI1
namespace led {
    DigitalOut Green(LED1);
    void Toggle() { Green = !Green; };
    
}
#endif

int main() {

    AvrISP* avrisp = new AvrISP(MOSI, MISO, SCK, RST);
    avrisp->set_spi_frequency(SPI_CLK);

    Stk500v1* stk500 = new Stk500v1(avrisp, AVRISP_TX, AVRISP_RX, AVRISP_BAUD);

    #ifndef __SPI1
    // TODO: error codes?
    Ticker* toggle_led_ticker = new Ticker();
    toggle_led_ticker->attach(&led::Toggle, 1.0f);
    #endif

    // Setup up a clock source to perform necromancy on dead avr's w/incorrect
    // fuse bytes. TODO: derive frequency at runtime
    FastPWM* pwm = new FastPWM(PWM_OUT);
    double_t freq = (1.0 / TARGET_CLK) * 1000000;
    pwm->period_us(freq);
    pwm->pulsewidth_us(freq/2.0);

    //Thread::wait(osWaitForever);
    while (1) {
        stk500->parse_cmd();
        Thread::wait(100);
    }
    
    return 0;
}

/* vim: set ft=pio: */
