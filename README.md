[![License BSD](https://img.shields.io/badge/License-BSD-lightgrey.svg)](/LICENSE)

# mbed-avrisp #

Emulates a STK500 (v1) programmer using the mbed platform.

### Quick Start ###

```sh
git clone https://gitlab.com/commando.brando2k/mbed-avrisp.git
cd mbed-avrisp
pio run -t upload
```

Then use avrdude to upload, e.g.:

```sh
avrdude -c stk500v1 -P /dev/ttyACM0 -p t85 -U flash:w:path/to/firmware.hex:a -v
```

### License ###

Copyright © 2017 B.A. Parker \<commando.brando2k@gmail.com\>

The work in this repository is licensed under the BSD 2-Clause license unless 
otherwise stated.

See [License](LICENSE) for details.
